package com.polahniuk.exception;

/**
 * Class extends {@link Exception}.
 * Overrides methods and constructors of {@link Exception}.
 * Works as class {@link Exception}.
 *
 * @author Ivan Polahniuk
 * @version beta 1
 */
public class MyException extends Exception {

    public MyException() {
    }

    public MyException(String message) {
        super(message);
    }

    public MyException(String message, Throwable cause) {
        super(message, cause);
    }

    public MyException(Throwable cause) {
        super(cause);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public void printStackTrace() {
        super.printStackTrace();
    }
}
