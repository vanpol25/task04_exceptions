package com.polahniuk.exception;

/**
 * Class extends {@link RuntimeException}.
 * Overrides methods and constructors of {@link RuntimeException}.
 * Works as class {@link RuntimeException}.
 *
 * @author Ivan Polahniuk
 * @version beta 1
 */
public class MyRuntimeException extends RuntimeException {

    public MyRuntimeException() {
    }

    public MyRuntimeException(String message) {
        super(message);
    }

    public MyRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public MyRuntimeException(Throwable cause) {
        super(cause);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }

    @Override
    public synchronized Throwable getCause() {
        return super.getCause();
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public void printStackTrace() {
        super.printStackTrace();
    }
}
