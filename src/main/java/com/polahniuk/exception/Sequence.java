package com.polahniuk.exception;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Random;

/**
 * Class works with {@link FileOutputStream}.
 * To create files with it's date of created.
 * Also files has it's date of closed which writes in method {@link Sequence#close}.
 * Class implements {@link AutoCloseable}.
 *
 * @author Ivan Polahniuk
 * @version beta 1
 */
public class Sequence implements AutoCloseable {

    private String createdDate;
    private String closedDate;
    private String name;
    private FileOutputStream file;

    public Sequence(String name) {
        this.name = name;
        createdDate = "Created at " + new Date().toString();
        try {
            file = new FileOutputStream(name + ".txt");
            file.write(name.getBytes());
            file.write(createdDate.getBytes());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            System.out.println("Created file " + name + ".txt");
        }
    }

    public void close() throws Exception {
        if (new Random().nextBoolean()) {
            throw new Exception();
        }
        closedDate = "Closed at " + new Date().toString();
        if (file != null) {
            file.write(closedDate.getBytes());
            file.close();
        }
    }

}
