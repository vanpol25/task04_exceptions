package com.polahniuk;

import com.polahniuk.exception.Sequence;

public class Main {
    public static void main(String[] args) {

        try (Sequence s = new Sequence("ho")) {
            System.out.println("Creating file ho.txt");
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (int i = 0; i < 5; i++) {
            try (Sequence s = new Sequence(i + "")) {
                System.out.println("Creating file " + i + ".txt");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}
